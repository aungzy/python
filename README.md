[![Gitpod ready-to-code](https://img.shields.io/badge/Gitpod-ready--to--code-blue?logo=gitpod)](https://gitpod.io/#https://gitlab.com/aungzy/python)

# python

Customized python image with additional packages.

## Active Branches

* 3.6.11
* 3.8.0
* 3.8

## Building with Gitlab CI

The image should be automatically built by Gitlab CI pipeline when pushed to origin.

> The Gitlab CI pipeline build job taks git branch or git tag as python version.
> The CI build will fail if you create a branch or tag which is not valid python version.

## Building locally

### Prerequisites

| Operating System | Requirements |
| --- | --- |
| Linux | [Docker Engine](https://docs.docker.com/engine/) |
| macOS | [Docker Desktop for Mac](https://docs.docker.com/docker-for-mac/install/) |
| Windows | [Docker Desktop for Windows](https://docs.docker.com/docker-for-windows/install/) |

### Using `docker` cli

* To build with default python version (Check the `ARG python_version` in the `Dockerfile` for default value), simply run:

  ```bash
  docker build -t localhost/python:3.6.11 .
  ```

* To override python version, add the docker build arg `--build-arg python_version=<version>` :

  ```bash
  docker build --build-arg python_version=3.6.11 -t localhost/python:3.6.11.
  ```

### Using `make` Command

* To build with default python version (which is the branch name), simply run:

  ```bash
  make
  ```

* To override python version, add `IMAGE_TAG=<version>` argument:
  ```bash
  make IMAGE_TAG=3.6.11
  ```
